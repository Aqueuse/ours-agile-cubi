from flask import Flask, request
#from flask_talisman import Talisman

from views.cubiAdmin import cubiAdmin_blueprint
from views.cubiAdmin import cubiItemShow_blueprint
from views.cubiAdmin import cubiItemCreate_blueprint
from views.cubiAdmin import cubiItemUpdate_blueprint
from views.cubiAdmin import cubiItemDelete_blueprint
from views.cubiAdmin import cubiCollectionRename_blueprint
from views.cubiAdmin import cubiCollectionDelete_blueprint
from views.cubiAdmin import cubiCollectionCreate_blueprint

from views.login import login_blueprint
from views.home import home_blueprint

import settings

app = Flask(__name__, static_folder='static')
#talisman = Talisman(app, content_security_policy=[])

app.config['UPLOAD_FOLDER'] = '/static/images'
app.config['SESSION_COOKIE_NAME'] = 'userSession'
app.config['SECRET_KEY'] = settings.SECRET

app.register_blueprint(login_blueprint)
app.register_blueprint(cubiAdmin_blueprint)
app.register_blueprint(cubiItemShow_blueprint)
app.register_blueprint(cubiItemCreate_blueprint)
app.register_blueprint(cubiItemUpdate_blueprint)
app.register_blueprint(cubiItemDelete_blueprint)
app.register_blueprint(cubiCollectionRename_blueprint)
app.register_blueprint(cubiCollectionDelete_blueprint)
app.register_blueprint(cubiCollectionCreate_blueprint)

app.register_blueprint(home_blueprint)


if __name__ == '__main__':
    app.run()
