import uuid

BASEURL = 'http://127.0.0.1:5000/'

SECRET = uuid.uuid4().hex[:16]

DATABASE = "oursAgile"
