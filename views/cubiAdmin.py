import json
from flask import Blueprint, render_template, request, redirect, make_response

import cubiDB
import settings

baseURL = settings.BASEURL
secret = settings.SECRET

cubiAdmin_blueprint = Blueprint('admin', __name__, )

cubiItemShow_blueprint = Blueprint('item_show', __name__, )
cubiItemCreate_blueprint = Blueprint('item_create', __name__, )
cubiItemUpdate_blueprint = Blueprint('item_update', __name__, )
cubiItemDelete_blueprint = Blueprint('item_delete', __name__, )

cubiCollectionRename_blueprint = Blueprint('collection_rename', __name__, )
cubiCollectionDelete_blueprint = Blueprint('collection_delete', __name__, )
cubiCollectionCreate_blueprint = Blueprint('collection_create', __name__, )


@cubiAdmin_blueprint.route('/admin', methods=['GET'])
def show_admin_page():
    if request.cookies.get('userID') == secret:
        collections_ids = []
        for element in cubiDB.get_all_collections_names():
            collections_ids.append(cubiDB.get_ids(element))
        return render_template(
            'cubiAdmin.html',
            collections_name=cubiDB.get_all_collections_names(),
            collections_ids=collections_ids,
            baseURL=baseURL
        )
    else:
        return redirect(baseURL)


# params = collection & id
@cubiItemShow_blueprint.route('/admin/item', methods=['GET'])
def show_item():
    if request.cookies.get('userID') == secret:
        collection_name = request.args.get('collection')
        id = request.args.get('id')
        item = cubiDB.get_item(collection_name, id)
        return item
    else:
        return redirect(baseURL)


@cubiItemCreate_blueprint.route('/admin/item/create', methods=['POST'])
def create_item():
    if request.cookies.get('userID') == secret:
        collection = request.form["collection"]
        cubiDB.duplicate_item(collection)
        return redirect('/admin')
    else:
        return redirect(baseURL)


@cubiItemUpdate_blueprint.route('/admin/item/update', methods=['POST'])
def update_item():
    if request.cookies.get('userID') == secret:
        json_data = json.loads(request.form["data"])
        collection = request.form["collection"]
        id = request.form["id"]
        del json_data["id"]
        cubiDB.update_item(collection, id, json_data)
        return redirect('/admin')
    else:
        return redirect(baseURL)


@cubiItemDelete_blueprint.route('/admin/item/delete', methods=['POST'])
def delete_item():
    if request.cookies.get('userID') == secret:
        collection = request.form["collection"]
        id = request.form["id"]
        cubiDB.remove_item(collection, id)
        return redirect('/admin')
    else:
        return redirect(baseURL)


@cubiCollectionRename_blueprint.route('/admin/collection/rename', methods=['POST'])
def rename_collection():
    if request.cookies.get('userID') == secret:
        old_collection_name = request.form["collection"]
        new_collection_name = request.form["new_collection_name"]
        cubiDB.rename_collection(old_collection_name, new_collection_name)
        return redirect('/admin')
    else:
        return redirect(baseURL)


@cubiCollectionDelete_blueprint.route('/admin/collection/delete', methods=['POST'])
def delete_collection():
    if request.cookies.get('userID') == secret:
        collection = request.form["collection"]
        cubiDB.remove_collection(collection)
        return redirect('/admin')
    else:
        return redirect(baseURL)


@cubiCollectionCreate_blueprint.route('/admin/collection/create', methods=['POST'])
def create_collection():
    if request.cookies.get('userID') == secret:
        collection_name = request.form["collection_name"]
        my_json = json.loads(request.form["data"])
        my_json['id'] = 0
        cubiDB.create_collection(collection_name, my_json)
        return redirect('/admin')
    else:
        return redirect(baseURL)
