from flask import Blueprint, render_template, request, redirect, make_response
import cubiDB, settings

baseURL = settings.BASEURL
secret = settings.SECRET

login_blueprint = Blueprint('login', __name__,)


@login_blueprint.route('/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    if request.method == 'POST':
        if cubiDB.validate_user(request.form['identifiant'], request.form['password']):
            response = make_response(redirect(baseURL+'admin'))
            response.set_cookie('userID', secret)
            return response
        else:
            return redirect(baseURL)