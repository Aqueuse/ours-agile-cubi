function show_item(collection, id) {
    const url = '/admin/item?collection=' + collection + '&id=' + id;
    let request = new Request(url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest', //Necessary to work with request.is_ajax()
        },
    });

    fetch(request)
        .then(response => {
            return response.json();
        })
        .then(function (json) {
            generate_item_form(collection, id, json);
        });
}


function generate_item_form(collection_name, id, json_data) {
    document.getElementById("form-container").style.display = "inline-block";
    document.getElementById("item-form").textContent = "";

    let input_collection = document.createElement("input");
    input_collection.type = "hidden";
    input_collection.name = "collection";
    input_collection.value = collection_name;
    document.getElementById("item-form").appendChild(input_collection);

    let input_id = document.createElement("input");
    input_id.type = "hidden";
    input_id.name = "id";
    input_id.value = id;
    document.getElementById("item-form").appendChild(input_id);

    let input_collection_data = document.createElement("input");
    input_collection_data.type = "hidden";
    input_collection_data.name = "data";
    input_collection_data.id = "data";
    input_collection_data.value = JSON.stringify(json_data);
    document.getElementById("item-form").appendChild(input_collection_data);

    editor.setValue(JSON.stringify(json_data, null, "\t"));

    let update_button = document.createElement("button");
    update_button.type = "submit";
    update_button.value = "update";
    update_button.innerText = "update";
    update_button.formAction = "/admin/item/update";
    document.getElementById("item-form").appendChild(update_button);

    let delete_button = document.createElement("button");
    delete_button.type = "submit";
    delete_button.value = "delete";
    delete_button.innerText = "delete";
    delete_button.formAction = "/admin/item/delete";
    document.getElementById("item-form").appendChild(delete_button);
}

function generate_collection_form() {
    document.getElementById("form-container").style.display = "inline-block";
    document.getElementById("item-form").textContent = "";

    let input_collection_name = document.createElement("input");
    input_collection_name.type = "text";
    input_collection_name.name = "collection_name";
    input_collection_name.placeholder = "collection name";
    document.getElementById("item-form").appendChild(input_collection_name);

    let input_collection_data = document.createElement("input");
    input_collection_data.name = "data";
    input_collection_data.type = "hidden";
    input_collection_data.id = "data";
    document.getElementById("item-form").appendChild(input_collection_data);

    editor.setValue("{}");

    let create_button = document.createElement("button");
    create_button.type = "submit";
    create_button.value = "create";
    create_button.innerText = "create";
    create_button.formAction = "/admin/collection/create";
    document.getElementById("item-form").appendChild(create_button);
}

function edit_field(field_id) {
    document.getElementById(field_id).removeAttribute("disabled");
}

function copy_json_data_to_form() {
    document.getElementById('data').value = editor.getValue();
}